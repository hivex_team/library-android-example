package com.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class AppBootReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.w("HIVEX", "AppBootReceiver");
        if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
            App.startHivex(context);
        }
    }
}
