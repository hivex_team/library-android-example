package com.ui;

import android.app.Application;
import android.app.Notification;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.app.NotificationCompat;

import com.hivex.client.Hivex;
import com.hivex.smartposition.SmartPosition;
import com.hivex.testapp.R;

public class App extends Application {

    public static void setForeground(Context aContext, boolean aForeground) {
        SharedPreferences sharedPref = aContext.getSharedPreferences("HivexDemo", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean("FOREGROUND", aForeground);
        editor.apply();
    }

    public static boolean isForeground(Context aContext) {
        SharedPreferences sharedPref = aContext.getSharedPreferences("HivexDemo", Context.MODE_PRIVATE);
        return sharedPref.getBoolean("FOREGROUND", false);
    }

    public static void startHivex(Context aContext) {
        /*
         * Define position mode and start service.
         * Example: position mode - HIGH_ACCURACY
         */
        Hivex.client().setPositionMode(SmartPosition.Mode.HIGH_ACCURACY);

        if (isForeground(aContext)) {
            /*
             * Start Hivex in foreground mode
             */
            Notification notification = new NotificationCompat.Builder(aContext)
                    .setContentTitle("HivexDemoSimple")
                    .setTicker("HivexDemoSimple")
                    .setContentText("HivexDemoSimple")
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .build();
            Hivex.startForeground(100, notification);  // magic number
        }
        else {
            /*
             * Start Hivex in background mode
             */
            Hivex.start();
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();

        /*
         * Only for demo and debugging
         */
        Hivex.initLog();

        /*
         * Init Hivex library:
         * - init
         * - auto-start service (if was started before)
         * - enable "keep-alive" guards (battery + alarms)
         * Please look to documentation.
         */
        Hivex.init(this);

        /*
         * Default start with application
         */
        startHivex(this);
    }
}
