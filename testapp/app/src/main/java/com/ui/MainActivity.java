package com.ui;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.hivex.client.Hivex;
import com.hivex.client.HivexPositionListener;
import com.hivex.testapp.R;
import com.hivex.testapp.BuildConfig;
import com.hivex.smartposition.SmartLocation;
import com.hivex.smartposition.SmartPosition;

public class MainActivity extends AppCompatActivity {

    private final static int permissionCode = 999;

    public static void closeApp(Context context) {
        android.os.Process.killProcess(android.os.Process.myPid());
    }

    public static void restartApp(Context context) {
        Intent mStartActivity = new Intent(context, MainActivity.class);
        int mPendingIntentId = 123456; // magic number
        PendingIntent mPendingIntent = PendingIntent.getActivity(context, mPendingIntentId,    mStartActivity, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager mgr = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 500, mPendingIntent);
        closeApp(context);
    }

    private static void checkPermissions(Activity context) {
        if (Build.VERSION.SDK_INT >= 23) {
            String[] permList = Hivex.permissions();
            boolean allIncluded = true;
            for (String SinglePerm: permList) {
                if (context.checkCallingOrSelfPermission(SinglePerm)==PackageManager.PERMISSION_DENIED) {
                    allIncluded = false;
                    break;
                }
            }
            if (!allIncluded) {
                ActivityCompat.requestPermissions(context, permList, permissionCode);
            }
        }
    }

    private static boolean isHivexPermission(String name) {
        for (String single : Hivex.permissions()) {
            if (single.equals(name)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode != permissionCode) {
            return;
        }
        for (int i = 0; i < permissions.length; i++) {
            if (isHivexPermission(permissions[i]) &&  grantResults[i] == PackageManager.PERMISSION_DENIED) {
                closeApp(this);
                return;
            }
        }
        restartApp(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(getTitle() + " - " + BuildConfig.VERSION_NAME);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /*
         * Check permissions for Hivex library and request missing ones
         */
        checkPermissions(this);

        /*
         * This example code shows how to catch data from Hivex
         */
        TextView view = (TextView) findViewById(R.id.textViewLog);
        view.append("Hivex supported: " + Hivex.isSupported() + "\r\n");
        Hivex.client().setPositionListener(new HivexPositionListener() {
            private boolean running = false;
            private SmartPosition.Mode mode = SmartPosition.Mode.fromInt(-1);
            @Override
            public void onStateChanged() {
                TextView view = (TextView) findViewById(R.id.textViewLog);
                if (running != Hivex.isRunning()) {
                    view.append("Hivex running: " + Hivex.isRunning() + "\r\n");
                    Log.w("HIVEX", "Hivex running: " + Hivex.isRunning());
                    running = Hivex.isRunning();
                }
                if (mode != Hivex.client().locationMode()) {
                    view.append("Hivex position mode: " + Hivex.client().locationMode() + "\r\n");
                    Log.w("HIVEX", "Hivex position mode: " + Hivex.client().locationMode());
                    mode = Hivex.client().locationMode();
                }
            }
            @Override
            public void onLocationChanged(SmartLocation smartLocation) {
                TextView view = (TextView) findViewById(R.id.textViewLog);
                view.append("Hivex new position: " + smartLocation + "\r\n");
                Log.w("HIVEX", "Hivex new position: " + smartLocation);
            }
        });

        /*
         * This example code for foreground control
         */
        final Switch switchForeground = findViewById(R.id.foreground);
        switchForeground.setChecked(App.isForeground(this));
        switchForeground.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                App.setForeground(MainActivity.this, isChecked);
                App.startHivex(MainActivity.this);
            }
        });
    }
}