﻿# Hivex SDK for Android

## Overview

HivexSDK is a Software Development Kit that implements the background anonymous collection of radio parameters and mobile network performance data. This data is used as an input for mobile network performance and end-user experience evaluation.

SDK implements low-energy consumption solution when positioning user. It can obtain accurate coordinates with minimum effect on device battery, especially when GPS is in use.

## Documentation

* [Changelog](Changelog.md)
* [API 1.45](https://s3.eu-central-1.amazonaws.com/hivexlibrary-doc.hivex-solutions.com/release/com/hivex/hivexlibrary/1.45/index.html)

## How it works

HivexSDK has a fully autonomous, asynchronous and isolated (from main application) implementation which is based on the Android Service. Service controlling and monitoring is done through the client interface (part of SDK).

The work cycle includes the following steps:


* Client initialization
* Binding to HivexSDK service
* Positioning initialization
* Addition of Listener(s) - optionally
* Provisioning customer feedback(s) - optionally
* Stop positioning
* Unbinding from HivexSDK service

Notes:


* The HivexSDK service is started in a separate process (`android: process = ":hivexlibrary"`) and it is hidden from third-party applications (`android: exported = "false"`)
* Internal service HivexSDK starts automatically when the first client is connected and turns off when the last has been disconnected (the bind method).
* Number of clients within one application is unlimited, however it is recommended to use only one. If more clients are used in the application, only one of them should start / stop the service and determine its parameters, while the others can define additional listeners.
* One client can have only one type listener
* The HivexSDK service can be turned on/off at any time
* Service parameters can be modified at any time
* It is recommended to disconnect from the service after it’s usage has been finished (for example, in onDestroy), since otherwise the service shutdown time (and, as a consequence, power consumption) is unknown.

The SDK automatically sets the requirements for the application:

```XML
<uses-permission android:name="android.permission.ACCESS_NETWORK_STATE"/>
<uses-permission android:name="android.permission.ACCESS_WIFI_STATE"/>
<uses-permission android:name="android.permission.INTERNET" />
<uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION" />
<uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />
```

## How to use

HivexSDK installation requires:

* Maven repository *access id* and *access key*
* Partner *application key*

Please contact Hivex team to get needed access data.

### Add SDK to project:

Define *aws_hivex_accessid* and *aws_hivex_accesskey* in project or global `gradle.properties`:

```
aws_hivex_accessid=<id>
aws_hivex_accesskey=<key>
```

Add to `build.gradle`

```
repositories {
    mavenCentral()
    maven {
        url "s3://hivexlibrary.hivex-solutions.com/release"
        credentials(AwsCredentials) {
            accessKey "${aws_hivex_accessid}"
            secretKey "${aws_hivex_accesskey}"
        }
    }
}

dependencies {
    compile 'com.hivex:hivexlibrary:<version>'
}

```

### Add to project application key/token:

`AndroidManifest.xml`

```XML
<application>
    <meta-data android:name="hivex_library_key" android:value="<your-application-token>" />
</application>
```

### Initialize SDK:

`MyApplication.java`

```java
public class MyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Hivex.init(this);
        Hivex.client().setPositionMode(SmartPosition.Mode.BALANCED);
        Hivex.start();
    }
}
```

Please look to example in `./example/simple` for more details

### Logging and testing
`MyApplication.java`

```java
public class MyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Hivex.init(this);
        Hivex.client().setPositionMode(SmartPosition.Mode.HIGH_ACCURACY);

        // here we add listener to log Hivex events
        Hivex.client().setPositionListener(new HivexPositionListener() {
            @Override
            public void onStateChanged() {
                Log.i("HIVEX", "Is running " + Hivex.client().isServiceRunning());
            }
            @Override
            public void onLocationChanged(SmartLocation aLocation) {
                Log.i("HIVEX", "New coordinate from " + aLocation.getSource());
            }
        });

        Hivex.start();
    }
}
```
