## 1.45 / 2019-12-08

* fix crash in 1.44

## 1.44 / 2019-12-05

* added support for custom auth token
* default wake-up timer changed to 20 minutes
* added new permissions:
    * android.permission.ACCESS_NETWORK_STATE
    * android.permission.ACCESS_WIFI_STATE
* Android SDK target version changed to 26
* added basic log for SDK (disabled by default)
* improved communication with gateway
* extended Wifi statistic
* more call events
* more accurate call drop detection in LTE
* updated testapp
* fixed several crashes

## 1.43 / 2019-03-24

* remove permission.READ_PHONE_STATE
* add minimal logging (Timber)
* fixed several bugs

## 1.42 / 2018-08-15

* fixed several crashes

### 1.41 / 2018-06-14

* fixed crash: Fatal Exception: java.lang.IllegalArgumentException: Receiver not registered
* fixed crash: Service has leaked IntentReceiver that was originally registered

### 1.40 / 2018-04-23

* Updated: PASSIVE mode as default for positioning
* Removed: enum ReleaseCause
* Added: Hivex.startForeground
* Removed: Hivex.isStarted
* Added(as deprecated): HivexClient.urlKPIMaps. Please use instead Hivex.urlKPIMaps
* Added: HivexSystemListener.onCallRelease. Indicates directly needed user feedback
* Updated: HivexSystemListener.onDownlinkDataRate and onUplinkDataRate. Added peak rate as param
* Removed: HivexSystemListener.onCallStart and HivexSystemListener.onCallEnd
* MeasTracer: 
** values collected by speed grouping
** more events in call tracer
** pilot pollution estimation
** fix bug with CID
** ARFCN support
** less load for PASSIVE mode
** more user confidentiality
** Wifi data rate tracking
* other fixes

### 1.31 / 2018-01-07

* hotfix: crash during service "wake-up"
* delete Hivex.wakeupByBattery()

### 1.30 / 2017-12-18

* add static client init
* rename HivexSystem to Hivex
* split Feedback to Feedback (only event: drop for e.g.) and Measurement(value: ping for e.g)
* move "support" methods from HivexClient to Hivex
* update all examples:
	* simplified example for basic use
	* simplified example for advanced use (with new api)
* SDK internals:
	* ECIO measurements

### 1.25 / 2017-09-30

Enhancements:

* add support private maven repo for hivex library (please look to demo)
* README update regards library integration

Bug fixes:

* Fix crash when android.permission.READ_PHONE_STATE was not added explicitly
  (in this case service will not start)

### 1.24 / 2017-08-27

Enhancements:

* add support for service activity time control

### 1.23 / 2017-08-01

Enhancements:

* added new PASSIVE mode for smartposition

Bug fixes:

* added back android.permission.READ_PHONE_STATE (needed for old versions of Android < 23)

### 1.22 / 2017-06-20

Enhancements:

* added new method HivexClient::kpiMapsURL
* demo target SDK updated to version 25

Removals

* removed usage of
	* android.permission.ACCESS_NETWORK_STATE
	* android.permission.READ_PHONE_STATE

### 1.21 / 2017-06-10
* bug fixing

### 1.20 / 2017-06-06

Enhancements:

* added call tracer (start,end) to HivexClient (new listener HivexSystemListener)
* added call release detection (new enum HivexSystem::ReleaseCause)
* added new method static FeedbackCause::fromReleaseCause, which will create feedback related to call release cause.
* enum HivexFeedback::Cause moved/renamed to HivexSystem::FeedbackCause
* updated demo application: added system listener demo
* removed usage of TelephonyManager::getDeviceId (critical code)
* added new method HivexClient::setUserData (only internal use)
* added 'hivex_library_key' to manifest file

### 1.18 / 2017-05-26

Enhancements:

* Hivex client now support setting of values before bind
* added new methods:
	* HivexClient::isServiceRunning
	* HivexClient::locationMode
	* HivexClient::locationState
	* HivexClient::lastKnownLocation
	* HivexPositionListener::onStateChanged (replacement to old one)
	* SmartLocation::isMock (replacement to old one)
* updated demo application:
	* added receivers for service wakeup (after boot, by battery signal)
	* more accurate service start control
	* added KPI Map API example (WebView based)

Removals

* removed methods:
	* HivexClient::isBounded
	* HivexClient::setPositionAllowMock
	* HivexClient::setPositionRawListener
	* HivexPositionListener::onPositioning
	* HivexPositionListener::onModeChanged
	* HivexPositionListener::onStateChanged(SmartPosition.State)
	* SmartLocation::getIsMock
* removed classes:
	* HivexPositionRawListener
